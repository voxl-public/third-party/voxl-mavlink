# voxl-mavlink

Mavlink standard and ModalAI custom messages bundled up into an ipk and deb.
No building is required, this just contains headers.

TODO build these headers from XML with mavgen.


## Build Instructions

1) Pull in ModalAI's custom fork of mavlink as a submodule


```bash
git submodule update --init --recursive
```

2) Make an ipk or deb package

```bash
voxl-mavlink:~$ ./make_package.sh deb
OR
voxl-mavlink:~$ ./make_package.sh ipk
```

This will make a new package file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the --timestamp argument to append the current data and time to the package version number in the debian package. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.


## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
voxl-mavlink$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.

```bash
(outside of docker)
voxl-mavlink$ ./deploy_to_voxl.sh ssh 192.168.1.123
```



## How to update this to pull new mavlink headers

Go to github and find the commit hash of whatever you want to pull (usually latest).

```
git submodule update --init --recursive
cd files/c_library_v2
git checkout fa15f88
cd ../../
```

now bump changelog and version in pkg/control
